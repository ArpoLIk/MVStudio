#include <iostream>
#include <math.h>
#include <string>

class SortArray {
private:
	static void Swap(int* swap, int index) {
		int c = swap[index + 1];
		swap[index + 1] = swap[index];
		swap[index] = c;
	}

	static void Swap(std::string* swap, int index) {
		std::string c = swap[index + 1];
		swap[index + 1] = swap[index];
		swap[index] = c;
	}

public:
	static void Sort(int* score, std::string* name, int size) {
		bool f = true;
		for (int j = 0; j < size; ++j) {
			f = false;
			for (int i = 0; i < size - 1 - j; ++i) {
				if (score[i] < score[i + 1]) {
					Swap(score, i);
					Swap(name, i);
					f = true;
				}
			}
			if (!f) {
				break;
			}
		}
	}

	void FillArray(int* score, std::string* name, int size) {
		for (int i = 0; i < size; ++i) {
			std::cout << "������� ��� ������������: ";
			std::cin >> name[i];
			std::cout << "������� ���-�� ��������� �����: ";
			std::cin >> score[i];
		}
	}
};