#include <iostream>
#include "Helpers.h"

class Animal {
public:
	Animal() {}
	virtual void Voice() {}
};

class Dog : public Animal {
public:
	Dog() {}
	void Voice() override{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal {
public:
	Cat() {}
	void Voice() override {
		std::cout << "May!" << std::endl;
	}
};

class Cow : public Animal {
public:
	Cow() {}
	void Voice() override {
		std::cout << "Moo!" << std::endl;
	}
};

int main()
{
	Animal* animals[] = {new Dog, new Cat, new Cow};
	Cat* cat = new Cat();

	for (Animal* sound: animals) {
		sound->Voice();
	}

}

